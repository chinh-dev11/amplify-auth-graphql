import React, { useState } from 'react';
import './App.css';

import { withAuthenticator } from 'aws-amplify-react'; // or 'aws-amplify-react-native';
import '@aws-amplify/ui/dist/style.css';

import { API, graphqlOperation } from 'aws-amplify';

// queries and mutations
const listTodos = `
  query listTodos {
    listTodos{
      items{
        id
        name
        description
      }
    }
  }`;

const addTodo = `
  mutation createTodo($name:String! $description: String!) {
    createTodo(input:{
      name:$name
      description:$description
    }){
      id
      name
      description
    }
  }`;

function App() {
  const [todo, settodo] = useState('');
  const [todos, settodos] = useState('');

  // methods to make GraphQL calls
  const todoMutation = async () => {
    settodo('');
    const todoDetails = {
      name: 'Party tonight!',
      description: 'Amplify CLI rocks!',
    };
    const newTodo = await API.graphql(graphqlOperation(addTodo, todoDetails));
    // console.log(JSON.stringify(newTodo.data));
    settodo(newTodo.data.createTodo);
  };
  const listQuery = async () => {
    settodos('');
    // console.log('listing todos');
    const allTodos = await API.graphql(graphqlOperation(listTodos));
    // console.log(JSON.stringify(allTodos.data.listTodos.items));
    settodos(allTodos.data.listTodos.items);
  };

  return (
    <div className="App">
      <p>GraphQL API</p>
      <button type="button" onClick={listQuery}>Query</button>
      <button type="button" onClick={todoMutation}>Mutation</button>
      {todo && <p>{`Added: ${todo.name} - ${todo.description}`}</p>}
      {todos && <ol>{todos.map((item) => <li key={item.id}>{`${item.name} - ${item.description}`}</li>)}</ol>}
    </div>
  );
}

export default withAuthenticator(App, true);
